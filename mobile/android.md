# Android

[Android](https://www.android.com/) is the
[#1](https://en.wikipedia.org/wiki/Usage_share_of_operating_systems#Mobile_devices)
mobile phone operating system and uses the
[Linux kernel](https://www.kernel.org/).

The majority of Android is free and open source through the
[Android Open Source Project (AOSP)](https://source.android.com/), but the phone
manufacturers frequently add proprietary apps and drivers that cannot be audited
or removed.

## Recommended phones and tablets

The Google line of phones all come with
[stock Android](https://developers.google.com/android/images) and can be
purchased with an unlocked bootloader.

## Alternative versions of Android

I would suggest using [LineageOS](https://lineageos.org/),
[CopperheadOS](https://copperhead.co/android/), or
[Replicant](https://www.replicant.us/).

[LineageOS](https://lineageos.org/) is Android without Google.  This is my
favorite version of Android.

[CopperheadOS](https://copperhead.co/android/) is Android without Google and
a hardened kernel for the privacy conscious.

[Replicant](https://www.replicant.us/) is Android without any proprietary
software.

Before changing the OS, make a backup of your files and OS.

## App Stores

Unlike iOS, there are many different app stores available for Android.

[F-Droid](https://f-droid.org/en/) is a free and open source app repository for
Android based phones.  Applications that may not have your best interests will
list their antifeatures.  Most applications found in F-Droid contain Richard
Stallman's Four Freedoms.

Most phones will come with Google Play Store preinstalled.  I do not trust
Google and I only use [F-Droid](https://f-droid.org/en/).
