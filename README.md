# SetupNotes
Notes on setting up systems in Educational Environments

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br>
Copyright © Michael McMahon 2015-2018.  Except where otherwise specified, the
text on [Setup Notes](https://github.com/TechnologyClassroom/SetupNotes/)
by Michael McMahon is licensed under the
[Creative Commons Attribution-ShareAlike License 4.0 (International) (CC-BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).

The one thing that has advanced my career with technology the most is taking
notes.

When I am about to do something new, I first write out what I am trying to do,
the problem that I am trying to solve, or the original error message.  I then
research the topic.  I take notes on the research and add the relevant links.
Before I follow a workflow from the Internet, I try to understand the workflow
as thoroughly as possible.  I document the process I use and add any notes about
what each step does.

If I messed up something, I should be able to refer to my notes and undo any
changes.  I then document that process.

Months later when I have forgotten the process, I can can look at my notes
instead of restarting over again at the research phase.

These are my notes.
  
- [Apple/](https://github.com/TechnologyClassroom/SetupNotes/tree/master/Apple)
- [BSD/](https://github.com/TechnologyClassroom/SetupNotes/tree/master/BSD)
- [GNULinux/](https://github.com/TechnologyClassroom/SetupNotes/tree/master/GNULinux)
  - [GNULinuxOnMacbooks.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/GNULinux/GNULinuxOnMacbooks.md)
    install instructions for dual-booting a white Macbook from 2009
- [WebBrowsers/](https://github.com/TechnologyClassroom/SetupNotes/tree/master/WebBrowsers)
- [Chromebook.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/Chromebook.md)
- [ESXi.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/ESXi.md)
- [git.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/git.md)
- [Github.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/Github.md)
- [management/](https://github.com/TechnologyClassroom/SetupNotes/tree/master/management)
  - [ansible.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/management/ansible.md)
- [mobile/](https://github.com/TechnologyClassroom/SetupNotes/tree/master/mobile)
  - [android.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/mobile/android.md)
- [Minetest.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/Minetest.md)
- [PXEchain.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/PXEchain.md)
  is deprecated.  See
  [PXE](https://github.com/TechnologyClassroom/PXE).
- [sphinx-docnotes.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/sphinx-docnotes.md)
- [thunderbird.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/thunderbird.md)
- [tmux.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/tmux.md)
- [Windows.md](https://github.com/TechnologyClassroom/SetupNotes/blob/master/Windows.md)
